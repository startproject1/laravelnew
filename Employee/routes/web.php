<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Home');
 });


Route::get('/add', 'ManageEmployeeController@add');
Route::post('/add', 'ManageEmployeeController@store');
Route::get('/edit/{id}', 'ManageEmployeeController@edit');
Route::post('/edit/{id}', 'ManageEmployeeController@update');
Route::get('/view', 'ManageEmployeeController@view');
Route::delete('/delete/{id}','ManageEmployeeController@destroy');
Route::get('/imageUpload','UploadFileController@uploadFile');
Route::post('/imageUpload','UploadFileController@imageUploadPost');