<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AppController;
use App\Http\Requests\EmployeeDetail;
class ManageEmployeeController extends AppController 
{
   
    /**
     * Show form to add record.
     *
     * @return \Illuminate\Http\Response
     */
    public function add() 
    {
         
        return view('application.AddEmployee');    
        
    }

    /**
     * Store new employee details to the database.
     *
     * @param \App\Http\Requests\EmployeeDetail $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeDetail $request) 
    {
    	 $data = $request->validated();//to validate the data
    	 if(!empty($data))
    	 {
               	 
    	 	DB::table('employee')->insert( ['name' => $data['empname'], 'designation' => $data['empdesg'],'employee_code' => $data['empid'],'department' => $data['empdept']] );
            return redirect('view')->with('success','User created successfully.');
            
    	 }
         else
         {
                   return view('application.AddEmployee');       
         }
    }

    /**
     * Update records already stored.
     *
     * @param \App\Http\Requests\EmployeeDetail $request, int $id
     * @return  \Illuminate\Http\Response
     */
    public function update(EmployeeDetail $request,$id)
    {
        $data = $request->validated();//to validate the data
         if(!empty($data))
         {
        

        DB::table('employee')->where('id', $id)->update( ['name' => $data['empname'], 'designation' => $data['empdesg'],'employee_code' => $data['empid'],'department' => $data['empdept']] );
            
         }
    
    return redirect('view')->with('success','User Updated successfully.');
    }

    /**
     * View the list of records already stored.
     *
     * 
     * @return  \Illuminate\Http\Response
     */
    public function view()
    {
        $employee = DB::select('select * from employee');
        return view('application.ViewEmployee', compact('employee'));
    }


    /**
     * Show form for updating specified record.
     *
     * @param int $id
     * @return  \Illuminate\Http\Response
     */
     public function edit($id)
    {

        $data = DB::select('select * from employee where id=?',[$id]);
        $employee = (array)$data[0];
        return view('application.AddEmployee', compact('employee'));
    }

    /**
     * Delete record.
     *
     * @param int $id
     * @return  \Illuminate\Http\Response
     */
     public function destroy($id)
     {
      DB::table('employee')->where('id', $id)->delete();
        return redirect('/view')->with('success', 'User has been deleted!!');
     }
}

?>