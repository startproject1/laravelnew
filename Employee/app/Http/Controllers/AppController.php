<?php
namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class AppController extends BaseController
{
    protected $redirectTo = '/';
    protected $request;
    protected $conditions = [];
    protected $viewVars = ['intPageNo' => 1, 'isPaging'=>1,'intRecsPerPage' => 10, 'arrPaging' => [], 'openFlag' => 'C', 'arrRecs' => []];
   
    public function __construct(Request $objRequest)
    {        
               
        $this->request      = $objRequest;

        $arrControllerParts = explode('\\', get_class($this));
        $strControllerName  = array_pop($arrControllerParts);

        $strModelClass = 'App\Models\\' . str_replace('Controller', 'Model', $strControllerName);
        
        
        if (class_exists($strModelClass)) {
            $this->model = new $strModelClass;
        }

        if (request('hdn_PageNo')) {

            $this->viewVars['intPageNo'] = request('hdn_PageNo');

        }
        
        if (request('hdn_IsPaging')!== null) {            
            
            $this->viewVars['isPaging'] = request('hdn_IsPaging');

        }
        
    }

    public function callAPI($postfields, $postmantoken) {
       //print_r($postfields);exit;
        $curl = curl_init();
        curl_setopt_array($curl, array(
           // CURLOPT_PORT => PORT,
            CURLOPT_URL => GRIEVANCE_SERVICE_URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "postman-token: " . $postmantoken,
                "authorization: Basic ".BASE_AUTH_TOKEN
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $dataResult = json_decode($response, true);
       // print_r($response);exit;
        return $dataResult;

    }
   
    
}
