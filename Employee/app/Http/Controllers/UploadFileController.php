<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UploadFileController extends Controller
{
     public function uploadFile()
    {
    	return view('application.FileUpload');
    }

    public function imageUploadPost()

    {
        request()->validate(['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images'), $imageName);
        //request()->image->move(public_path('images'), $imageName)->chmod($imageName,'777');
        return back()->with('success','You have successfully upload image.')->with('image',$imageName);

    }
}