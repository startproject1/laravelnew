<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    
/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'employee';

/**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];
/**
     * Fillable fields for a employee profile.
     *
     * @var array 
     */
    protected $fillable = [
        'designation',
        'employee_code',
        'department',
    ];
 /**
     * Typecasting for each column.
     *
     * @var array
     */
    protected $casts = [
        'designation'    => 'string',
        'employee_code'  => 'string',
        'department'     => 'string',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
