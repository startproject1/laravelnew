@extends('layouts.App')

@section('title', 'Employee Information System')

@section('content')
<div class="container" style="margin-top: 40px;">
	<div class="row">
		<div class="col-xs-12 col-sm-6 offset-sm-3 col-md-6 offset-md-3 ">
		<h1>Employee Information System</h1>
			<div class="col-sm-6" style="float:left;">
				<a href="{{URL::to('add')}}">Add Employee</a>
			</div>
			<div class="col-sm-6" style="float: right;">
				<a href="{{URL::to('view')}}">View Employees</a>
			</div>
		</div>
	</div>
</div>
@endsection