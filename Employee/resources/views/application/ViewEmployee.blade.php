@extends('layouts.App')

@section('content')
<!-- TO CHECK IF MESSAGE IS SET IN SESSION AND ALERT THE MESSAGE -->
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
<div class="container">
    <div class="row" >
        <div class="col-sm-12" >
                <div class="col-sm-2" style="float:right;"><a href="{{URL::to('add')}}">Add Employee</a></div>
                <div class="col-sm-2" style="float:right;"><a href="{{URL::to('')}}">Home</a></div>
                <div class="col-sm-8" style="float:left;text-align: center;"><h3>View Details</h3></div>
            </div>
        
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td>Name</td>
                        <td>Designation</td>
                        <td>Employee Id</td>
                        <td>Department</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @if($employee)
                  @foreach($employee as $emp)
                <tr>
                    <td>{{$emp->name }}</td>
                    <td>{{$emp->designation}}</td>
                    <td>{{$emp->employee_code}}</td>
                    <td>{{$emp->department}}</td>
                    <td>
                        <form action="{{ action('ManageEmployeeController@destroy', $emp->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                        <a href="edit/{{$emp->id}}">edit</a>
                        @csrf
                    </td>
                </tr>
                @endforeach 
                @else
                <div class="noRecord">No records Found</div>
                @endif  
                </tbody>
            </table>
    </div>
</div>

@endsection