
@extends('layouts.App')

@section('content')
   

        @if ($errors->any())
	        <div class="alert alert-danger">
	            <ul>
	                @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	                @endforeach
	            </ul>
	        </div>
        @endif
        <!-- TO CHECK IF MESSAGE IS SET IN SESSION AND ALERT THE MESSAGE -->
        @if (session('ErrorMessage'))
            <div class="alert alert-danger">
                {{ session('ErrorMessage') }}
            </div>
        @endif
    <!-- THIS WILL FETCH THE DATA IN UPDATE CASE AND AUTO POPULATE IT INTO THE TEXT FIELDS -->
        
      
    <!-- TO CHECK IF DATA IS PRESENT AND CHANGE THE PAGE TITLE ACCORDINGLY -->    
		@if(isset($id) &&  $id>0 )
	      @section('title', 'Update')
	 	@else
	      @section('title', 'Add')      
	  	@endif    

	  	<style type="text/css">
	  		.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9 {
    float: left;
}
	  	</style>
<div class="container">
    <div class="row" >
		<div class="col-sm-12" >
	        <div class="col-sm-2" style="float:right;"><a href="{{URL::to('view')}}">View Employee</a></div>
	        <div class="col-sm-2" style="float:right;"><a href="{{URL::to('')}}">Home</a></div>
	        <div class="col-sm-8" style="float:left;text-align: center;"><h3>Add Details</h3></div>
	        	<!-- FORM FOR ADD AND UPDATE CASE STARTS --> 
			<form method="post" action="" autocomplete="off" enctype="multipart/form-data">
				{{csrf_field()}}

				<div class="dataTable">
					<div class="form-group">
						<label class="col-lg-2 control-label ">Emp Name</label> <span class="float-left">:</span>
						<div class="col-lg-4"><input type="text" class="form-control" name="empname" value="{{$employee['name']??$employee['name']??old('empname')}}"></div>
						
						<div class="clearfix"></div>	
					</div>

					<div class="form-group">
						<label class="col-lg-2">Designation</label><span class="float-left">:</span>
						<div class="col-lg-4"><input type="text" class="form-control" name="empdesg" value="{{$employee['designation']??$employee['designation']??old('empdesg')}}">
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="form-group">
						<label class="col-lg-2">Employee Id</label><span class="float-left">:</span>
						<div class="col-lg-4"><input type="text" class="form-control" name="empid" value="{{$employee['employee_code']??$employee['employee_code']??old('empid')}}"></div>
						<div class="clearfix"></div>
					</div>

					<div class="form-group">
						<label class="col-lg-2">Department</label><span class="float-left">:</span>
						<div class="col-lg-4"><input type="text" class="form-control" name="empdept" value="{{$employee['department']??$employee['department']??old('empdept')}}"></div>
						<div class="clearfix"></div>
					</div>
					
					<div class="form-group">

						<input class="btn btn-success" type="submit" class="btn btn-success" name="add" value="{{isset($employee)?'Update':'Add'}}">
						<input class="btn btn-warning" type="reset" class="btn btn-success" name="reset" value="{{isset($employee)?'Cancel':'Reset'}}">
					</div>

				</div>			
			</form>
	    </div>
	

	</div>
</div>
	<!-- END FORM --> 
@endsection